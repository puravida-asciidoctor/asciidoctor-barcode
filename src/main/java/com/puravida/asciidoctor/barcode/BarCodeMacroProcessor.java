package com.puravida.asciidoctor.barcode;

import org.asciidoctor.ast.ContentNode;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.*;

import java.awt.*;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Name("barcode")
@Format(FormatType.LONG)
@PositionalAttributes({"url","width","height"})
public class BarCodeMacroProcessor extends BlockMacroProcessor {



    @Override
    public Object process(StructuralNode parent, String target, Map<String, Object> attributes) {
        Map<String,Object> attr = new HashMap<>();
        BarcodeProcessor barcodeProcessor = new BarcodeProcessor();
        File output = barcodeProcessor.generateBarCode(parent,target,attributes, attr);
        if( output == null)
            return null;
        return createBlock(parent,"image","", attr);
    }
}
