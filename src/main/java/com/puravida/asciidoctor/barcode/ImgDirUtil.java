package com.puravida.asciidoctor.barcode;

import org.asciidoctor.ast.ContentNode;

import java.io.File;

public class ImgDirUtil {

    static File imageDir(ContentNode parent){
        File fImageDir=null;

        Object out_dir = parent.getDocument().getAttributes().get("imagesoutdir");

        if( out_dir != null ){
            fImageDir = new File(out_dir.toString());
        }else{
            Object to_dir = parent.getDocument().getOptions().get("to_dir");
            String imagedir = parent.getDocument().getAttributes().get("imagesdir") != null ?
                    parent.getDocument().getAttributes().get("imagesdir").toString() : "images";
            File root = new File(to_dir.toString());
            fImageDir = new File( root, imagedir);
        }
        fImageDir.mkdirs();

        return fImageDir;
    }

    static String imagePath(ContentNode parent, File fImage){
        String imagedir = parent.getDocument().getAttributes().get("imagesdir") != null ?
                "." : "images";
        return imagedir+"/"+fImage.getName();
    }

}
