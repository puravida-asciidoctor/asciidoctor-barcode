package com.puravida.asciidoctor.barcode;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class BarCodeInlineTests {

    @Test
    public void multiples1D() {
        Map<String,Object> attributes = new HashMap<String,Object>();

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);
        options.setToDir(new File("build").getAbsolutePath());
        options.setOption("mkdirs", "true");
        options.setOption("imagesoutdir", "build");
        options.setToFile("multiples1d.html");

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        asciidoctor.convert(String.format(
                "= My document\n\n" +
                        "preambulo \n"+
                        "barcode:code128[1234567890123,300,300]\n"+
                        "barcode:ean8[12345678,300,300]\n"+
                        "barcode:ean13[0000642109529,300,300]\n"+
                        "barcode:upca[012345678905,300,300]\n"+
                        "barcode:upce[01240136,300,300]\n"+
                        "footer \n"+
                        ""), options);
    }

    @Test
    public void multiples2D() {
        Map<String,Object> attributes = new HashMap<String,Object>();
        attributes.put(Attributes.IMAGESDIR,"images");


        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);
        options.setToDir(new File("build").getAbsolutePath());
        options.setOption("mkdirs", "true");
        options.setOption("imagesoutdir", "build");
        options.setToFile("multiples2d.html");

        options.setAttributes(attributes);
        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        asciidoctor.convert(String.format(
                "= My document\n\n" +
                        "preambulo \n"+
                        "barcode:qrcode[http://puravida-software.com,300,300]\n"+
                        "barcode:dmatrix[http://puravida-software.com,300,300]\n"+
                        "barcode:pdf417[http://puravida-software.com,300,300]\n"+
                        "barcode:qrcode[http://puravida-software.com/,300,300,backimg=\"logo.jpg\"]\n"+
                        "barcode:qrcode[http://puravida-software.com/,300,300,stamp=\"logo.jpg\"]\n"+
                        "footer \n"+
                        ""), options);
    }


}
